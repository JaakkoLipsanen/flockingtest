using System;
using Flai;
using Flai.Graphics;
using FlockingTest.Screens;

namespace FlockingTest
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class FlockingGame : FlaiGame
    {
        public FlockingGame()
        {
            base.InactiveSleepTime = TimeSpan.Zero;
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _screenManager.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            _screenManager.Draw(graphicsContext);
        }

        protected override void InitializeGraphicsSettings()
        {
            _graphicsDeviceManager.PreferredBackBufferWidth = 1920;
            _graphicsDeviceManager.PreferredBackBufferHeight = 1080;
            _graphicsDeviceManager.PreferMultiSampling = true;
        }

        protected override void AddInitialScreens()
        {
            _screenManager.AddScreen(new GameplayScreen());
        }
    }

    #region Entry Point

    internal static class Program
    {
        static void Main()
        {
            FlaiGame.Run<FlockingGame>();
        }
    }

    #endregion
}
