﻿using Flai;
using Flai.General;
using System.Collections.Generic;
using Flai.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace FlockingTest.Model
{
    public class World
    {
        private readonly BoidManager _boidManager;
        private readonly EditableTileMap<bool> _tileMap;
        private bool? _isAdding = null;
        private Vector2? _target;

        public ITileMap<bool> TileMap
        {
            get { return _tileMap; }
        }

        public IEnumerable<Boid> Boids
        {
            get { return _boidManager; }
        }

        public static int Width
        {
            get { return FlaiGame.Current.ScreenSize.Width; }
        }

        public static int Height
        {
            get { return FlaiGame.Current.ScreenSize.Height; }
        }

        public Vector2? Target
        {
            get { return _target; }
        }

        public bool IsFlockingEnabled { get; set; }

        public World()
        {
            this.IsFlockingEnabled = true;

            int width = 1920 / Tile.Size;
            int height = 1920 / Tile.Size;
            _tileMap = new EditableTileMap<bool>(new bool[width * height], width, height);
            _boidManager = new BoidManager(this);
        }

        public void Update(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Right))
            {
                _target = updateContext.InputState.MousePosition;
            }
            else
            {
                _target = null;
            }

            if (updateContext.InputState.IsNewKeyPress(Keys.Enter))
            {
                this.IsFlockingEnabled = !this.IsFlockingEnabled;
            }

            _boidManager.Update(updateContext);

            // this stuff doesnt belong here but whatever
            if (updateContext.InputState.IsMouseButtonPressed(MouseButton.Left) && updateContext.ScreenArea.Contains(updateContext.InputState.MousePosition))
            {
                if (!_isAdding.HasValue)
                {
                    _isAdding = !_tileMap[updateContext.InputState.MousePosition / Tile.Size];
                }

                _tileMap[updateContext.InputState.MousePosition / Tile.Size] = _isAdding.Value;
            }
            else
            {
                _isAdding = null;
            }

            if (updateContext.InputState.IsNewKeyPress(Keys.Space))
            {
                _tileMap.Clear();
            }
        }
    }
}
