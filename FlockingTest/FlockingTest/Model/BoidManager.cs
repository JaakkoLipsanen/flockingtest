﻿using Flai;
using Microsoft.Xna.Framework;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace FlockingTest.Model
{
    public class BoidManager : IEnumerable<Boid>
    {
        private const float MaxFlockDistance = 120 / 16f * Boid.Size;
        private const float BoidSpeed = 300 / 16f * Boid.Size;
        private const float SeperationDistance = 45 / 16f * Boid.Size;

        private readonly List<Boid> _boids = new List<Boid>();
        private readonly World _world;

        private float _cohesion = 2.5f;
        private float _alignment = 5;
        private float _separation = 5;
        private float _target = 5;
        private float _random = 0.5f;

        public BoidManager(World world)
        {
            _world = world;
            for (int i = 0; i < 100; i++)
            {
                _boids.Add(this.CreateBoid());
            }
        }

        public void Update(UpdateContext updateContext)
        {
            this.HandleInput(updateContext);
            if (_world.IsFlockingEnabled)
            {
                this.SimulateFlocking(updateContext);
            }

            this.UpdatePositions(updateContext);
        }

        private void HandleInput(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsNewKeyPress(Keys.Up) && _boids.Count > 49)
            {
                _boids.RemoveRange(_boids.Count - 50, 49);
            }
            else if (updateContext.InputState.IsNewKeyPress(Keys.Down) && _boids.Count < 360)
            {
                for (int i = 0; i < 50; i++)
                {
                    _boids.Add(this.CreateBoid());
                }
            }
        }

        private void SimulateFlocking(UpdateContext updateContext)
        {
            foreach (Boid boid in _boids)
            {
                Vector2 cohesion = this.CalculateCohesion(boid) * _cohesion;
                Vector2 alignment = this.CalculateAlignment(boid) * _alignment;
                Vector2 separation = this.CalculateSeparation(boid) * _separation;
                Vector2 target = _world.Target.HasValue ? Vector2.Normalize(_world.Target.Value - boid.Position) * _target : Vector2.Zero;
                Vector2 random = FlaiAlgorithms.GenerateRandomVector2(new Range(-1, 1), new Range(-1, 1)) * _random;
                Vector2 wallAvoidance = this.CalculateWallAvoidance(boid) * 20;

                boid.Velocity += (cohesion + alignment + separation + target + random + wallAvoidance) * BoidSpeed * updateContext.DeltaSeconds;
                if (boid.Velocity != Vector2.Zero)
                {
                    boid.Velocity = Vector2.Normalize(boid.Velocity) * BoidSpeed;
                }
            }
        }

        private void UpdatePositions(UpdateContext updateContext)
        {
            foreach (Boid boid in _boids)
            {
                const float OldVelocityInfluence = 0.5f;
                const float NewVelocityInfluence = 0.5f;

                boid.Position += boid.Velocity * updateContext.DeltaSeconds;
                if (boid.Position.X < 0)
                {
                    boid.Position.X = World.Width + boid.Position.X;
                }
                else if (boid.Position.X > World.Width)
                {
                    boid.Position.X -= World.Width;
                }

                if (boid.Position.Y < 0)
                {
                    boid.Position.Y = World.Height + boid.Position.Y;
                }
                else if (boid.Position.Y > World.Height)
                {
                    boid.Position.Y -= World.Height;
                }
            }
        }

        private Boid CreateBoid()
        {
            return new Boid
            {
                Position = FlaiAlgorithms.GenerateRandomVector2(new Range(0, World.Width), new Range(0, World.Height)),
                Velocity = FlaiAlgorithms.GenerateRandomVector2(new Range(-400, 400), new Range(-400, 400))
            };
        }

        #region Flocking Behavior functions

        private Vector2 CalculateWallAvoidance(Boid boid)
        {
            const int MaxWallDistance = 2;
            Vector2 sum = Vector2.Zero;

            Vector2i tileCoord = new Vector2i(boid.Position / Tile.Size);
            for (int x = FlaiMath.Max(0, tileCoord.X - MaxWallDistance); x <= FlaiMath.Min(_world.TileMap.Width - 1, tileCoord.X + MaxWallDistance); x++)
            {
                for (int y = FlaiMath.Max(0, tileCoord.Y - MaxWallDistance); y <= FlaiMath.Min(_world.TileMap.Height - 1, tileCoord.Y + MaxWallDistance); y++)
                {
                    if (_world.TileMap[x, y])
                    {
                        float difference = Vector2.Distance(boid.Position, new Vector2(x, y) * Tile.Size) / Tile.Size / FlaiMath.Sqrt(MaxWallDistance * MaxWallDistance * 2); // trigonometry
                        float adjusted = 1 / (difference * difference + 1);
                        sum += (tileCoord - new Vector2i(x, y)) * FlaiMath.Max(0, adjusted);
                    }
                }
            }

            if (sum.LengthSquared() > 1)
            {
                return Vector2.Normalize(sum);
            }

            return sum;
        }

        private Vector2 CalculateSeparation(Boid boid)
        {
            Vector2 sum = Vector2.Zero;
            int count = 0;
            foreach (Boid other in _boids)
            {
                float distance = Vector2.Distance(boid.Position, other.Position);
                if (boid != other && distance < BoidManager.SeperationDistance)
                {
                    sum += Vector2.Normalize(boid.Position - other.Position) * FlaiMath.Pow(1 - distance / BoidManager.SeperationDistance, 2);
                    count++;
                }
            }

            if (count == 0 || sum == Vector2.Zero)
            {
                return Vector2.Zero;
            }

            return Vector2.Normalize(sum / count);
        }

        private Vector2 CalculateAlignment(Boid boid)
        {
            Vector2 sum = Vector2.Zero;
            int count = 0;
            foreach (Boid other in _boids)
            {
                float distance = Vector2.Distance(boid.Position, other.Position);
                if (boid != other && distance < BoidManager.MaxFlockDistance)
                {
                    sum += Vector2.Normalize(other.Velocity) * (BoidManager.MaxFlockDistance / distance);
                    count++;
                }
            }

            if (count == 0 || sum == Vector2.Zero)
            {
                return Vector2.Zero;
            }

            return Vector2.Normalize(sum / count);
        }

        private Vector2 CalculateCohesion(Boid boid)
        {
            Vector2 sum = Vector2.Zero;
            int count = 0;
            foreach (Boid other in _boids)
            {
                float distance = Vector2.Distance(boid.Position, other.Position);
                if (boid != other && distance < BoidManager.MaxFlockDistance && distance > BoidManager.SeperationDistance)
                {
                    sum += (other.Position - boid.Position) * FlaiMath.Pow((distance - BoidManager.SeperationDistance) / (BoidManager.MaxFlockDistance - BoidManager.SeperationDistance), 2);
                    count++;
                }
            }

            if (count == 0 || sum == Vector2.Zero)
            {
                return Vector2.Zero;
            }

            sum /= count;
            return Vector2.Normalize(sum);
        }

        #endregion

        #region IEnumerable<T>

        public IEnumerator<Boid> GetEnumerator()
        {
            return _boids.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _boids.GetEnumerator();
        }

        #endregion
    }
}
