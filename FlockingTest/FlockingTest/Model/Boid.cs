﻿using Microsoft.Xna.Framework;

namespace FlockingTest.Model
{
    public class Boid
    {
        public const float Size = 12;

        public Vector2 Position;
        public Vector2 Velocity;
    }
}
