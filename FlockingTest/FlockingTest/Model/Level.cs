﻿using Flai;

namespace FlockingTest.Model
{
    public class Level
    {
        private readonly World _world;

        public World World
        {
            get { return _world; }
        }

        private Level(World world)
        {
            _world = world;
        }

        public void Update(UpdateContext updateContext)
        {
            _world.Update(updateContext);
        }

        public static Level Generate()
        {
            return new Level(new World());
        }
    }
}
