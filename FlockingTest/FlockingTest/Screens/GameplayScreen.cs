﻿using Flai;
using Flai.Graphics;
using Flai.ScreenManagement;
using FlockingTest.Model;
using FlockingTest.View;

namespace FlockingTest.Screens
{
    public class GameplayScreen : GameScreen
    {
        private readonly Level _level;
        private readonly LevelRenderer _levelRenderer;

        public GameplayScreen()
        {
            Cursor.IsVisible = true;
            _level = Level.Generate();
            _levelRenderer = new LevelRenderer(_level);
        }

        protected override void LoadContent()
        {
            _levelRenderer.LoadContent();
        }

        protected override void Update(UpdateContext updateContext, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            _level.Update(updateContext);
            _levelRenderer.Update(updateContext);
        }

        protected override void Draw(GraphicsContext graphicsContext)
        {
            _levelRenderer.Draw(graphicsContext);
        }
    }
}
