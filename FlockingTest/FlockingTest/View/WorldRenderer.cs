﻿using Flai;
using Flai.Graphics;
using FlockingTest.Model;
using Microsoft.Xna.Framework;

namespace FlockingTest.View
{
    public class WorldRenderer : FlaiRenderer
    {
        private readonly World _world;

        public WorldRenderer(World world)
        {
            _world = world;
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.DrawStringCentered(graphicsContext.FontContainer.DefaultFont, "FLOCKING", new Vector2(World.Width / 2f, 64), Color.DimGray * 0.25f);
            graphicsContext.SpriteBatch.DrawStringCentered(
                graphicsContext.FontContainer.DefaultFont,
                "FLOCKING " + (_world.IsFlockingEnabled ? "ENABLED" : "DISABLED"),
                new Vector2(World.Width/2f, 144),
                _world.IsFlockingEnabled ? Color.Green * 0.5f : Color.Red,
                0, 0.25f);

            for (int y = 0; y < _world.TileMap.Height; y++)
            {
                for (int x = 0; x < _world.TileMap.Width; x++)
                {
                    if (_world.TileMap[x, y])
                    {
                        graphicsContext.PrimitiveRenderer.DrawRectangle(new Rectangle(x * Tile.Size, y * Tile.Size, Tile.Size, Tile.Size), Color.Black);
                    }
                }
            }

            foreach (Boid boid in _world.Boids)
            {
                graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, boid.Position, Color.Blue, FlaiMath.GetAngle(boid.Velocity), Boid.Size);
                if (boid.Position.X < Boid.Size)
                {
                    graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, World.Width * Vector2.UnitX + boid.Position, Color.Blue, FlaiMath.GetAngle(boid.Velocity), Boid.Size);
                }
                else if (boid.Position.X > World.Width - Boid.Size)
                {
                    graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, boid.Position - World.Width * Vector2.UnitX, Color.Blue, FlaiMath.GetAngle(boid.Velocity), Boid.Size);
                }

                if (boid.Position.Y < Boid.Size)
                {
                    graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, World.Height * Vector2.UnitY + boid.Position, Color.Blue, FlaiMath.GetAngle(boid.Velocity), Boid.Size);
                }
                else if (boid.Position.Y > World.Height - Boid.Size)
                {
                    graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, boid.Position - World.Height * Vector2.UnitY, Color.Blue, FlaiMath.GetAngle(boid.Velocity), Boid.Size);
                }
            }

            if (_world.Target.HasValue)
            {
                graphicsContext.PrimitiveRenderer.DrawRectangle(_world.Target.Value, 24f, Color.Red);
            }
        }
    }
}
