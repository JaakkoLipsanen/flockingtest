﻿using Flai;
using Flai.Graphics;
using FlockingTest.Model;
using Microsoft.Xna.Framework;

namespace FlockingTest.View
{
    public class LevelRenderer : FlaiRenderer
    {
        private readonly Level _level;
        private readonly WorldRenderer _worldRenderer;

        public LevelRenderer(Level level)
        {
            _level = level;
            _worldRenderer = new WorldRenderer(level.World);
        }

        protected override void LoadContentInner()
        {
            _worldRenderer.LoadContent();
        }

        protected override void UnloadInner()
        {
            _worldRenderer.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _worldRenderer.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.GraphicsDevice.Clear(Color.White);

            graphicsContext.SpriteBatch.Begin();
            _worldRenderer.Draw(graphicsContext);
            graphicsContext.SpriteBatch.End();
        }
    }
}
